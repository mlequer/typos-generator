<?php
/**
 * Copyright (c) 2020. >Michel Le Quer michel@mlequer.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

namespace MLequer\Generator;


use PHPUnit\Framework\TestCase;
use PHPUnit\Framework\StaticAnalysis\HappyPath\AssertSame;

class TypoGeneratorTest extends TestCase
{

    private $allOptions = [
        'wrongKeys' => true,
        'missedChars' => true,
        'transposedChars' => true,
        'doubleChars' => true,
        'flipBits' => true,
        'generateHomophones' => true,
    ];

    private $noOptions = [
        'wrongKeys' => false,
        'missedChars' => false,
        'transposedChars' => false,
        'doubleChars' => false,
        'flipBits' => false,
        'generateHomophones' => false,
    ];

    public function testGetAllGetTypos()
    {
        $generator = new TypoGenerator($this->allOptions);
        $res = $generator->getTypos('Homophones');
        sort($res);
        $expected = $this->loadExpectedResults();
        sort($expected);

        $this->assertEquals($expected, $res);

    }

     public function testGetNoTypos()
     {
         $generator = new TypoGenerator($this->noOptions);
         $res = $generator->getTypos('Homophones');

         $this->assertEquals([],$res);

     }


    private function loadExpectedResults(){
        return array_map('trim',file(__DIR__.'/../../var/test.txt'));
    }
}
