<?php
/**
 * Copyright (c) 2020. >Michel Le Quer michel@mlequer.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

namespace MLequer\Command;

use PHPUnit\Framework\TestCase;
use Symfony\Component\Console\Application;
use Symfony\Component\Console\Tester\CommandTester;

class TyposCommandTest extends TestCase
{
    public function testExecute()
    {
        $application = new Application();
        $application->add(new TyposCommand());
        $command = $application->find('typos:generate');
        $commandTester = new CommandTester($command);
        $commandTester->execute([
            // pass arguments to the helper
            'word' => 'Homophones',
            '--wrong-keys' => true,
            '--missed-chars' => true,
            '--transposed-chars' => true,
            '--double-chars' => true,
            '--flip-bits' => true,
            '--generate-homophones' => true,

            // prefix the key with two dashes when passing options,
            // e.g: '--some-option' => 'option_value',
        ]);

        // the output of the command in the console
        $output = $commandTester->getDisplay();
        $res = explode(PHP_EOL, $output);
        $res = array_filter($res, 'strlen');
        sort($res);

        $expected = $this->loadExpectedResults();

        sort($expected);

        $this->assertSame($expected, $res);

        // ...
    }

    private function loadExpectedResults()
    {
        return array_map('trim', file(__DIR__.'/../../var/test.txt'));
    }
}
