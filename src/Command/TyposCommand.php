<?php

/**
 * Copyright (c) 2020. >Michel Le Quer michel@mlequer.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

namespace MLequer\Command;

use MLequer\Generator\TypoGenerator;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * The setup commmand
 *
 * @author michel
 */
class TyposCommand extends Command
{
    protected static $defaultName = 'typos:generate';

    protected function configure()
    {
        $this
            ->setDescription('Return generated typos for a word')
            ->addArgument('word', InputArgument::REQUIRED, 'Word')
            ->addOption('wrong-keys', 'w', InputOption::VALUE_NONE, 'generate wrong key typos')
            ->addOption('missed-chars', 'm', InputOption::VALUE_NONE, 'generate missed chars typos')
            ->addOption('transposed-chars', 't', InputOption::VALUE_NONE, 'generate transposed chars typos')
            ->addOption('double-chars', 'd', InputOption::VALUE_NONE, 'generate double chars typos')
            ->addOption('flip-bits', 'f', InputOption::VALUE_NONE, 'generate flip-bits typos')
            ->addOption('generate-homophones', 'g', InputOption::VALUE_NONE, 'generate homophones typos');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $options = [
            'wrongKeys' => $input->getOption('wrong-keys'),
            'missedChars' => $input->getOption('missed-chars'),
            'transposedChars' => $input->getOption('transposed-chars'),
            'doubleChars' => $input->getOption('double-chars'),
            'flipBits' => $input->getOption('flip-bits'),
            'generateHomophones' => $input->getOption('generate-homophones'),
        ];

        $generator = new TypoGenerator($options);
        $typos = $generator->getTypos($input->getArgument('word'));
        $output->writeln($typos);

        return 0;
    }
}
