<?php
/**
 * Copyright (c) 2020. >Michel Le Quer michel@mlequer.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

namespace MLequer\Generator;

use Symfony\Component\OptionsResolver\OptionsResolver;
use Traversable;

/**
 * TypoGenerator Class.
 *
 * A utility class that generate typos.
 *
 * @author "Michel Le Quer <michel@mlequer.com>"
 * @since  0.1
 * */
class TypoGenerator
{

    /**
     * Default options, only generate wrong keys
     *
     * @var array
     */
    private $options = [
        'wrongKeys' => true,
        'missedChars' => false,
        'transposedChars' => false,
        'doubleChars' => false,
        'flipBits' => false,
        'generateHomophones' => false,
    ];

    private $generatorMap = [
        'wrongKeys' => WrongKeyTyposGenerator::class,
        'missedChars' => MissedCharTyposGenerator::class,
        'transposedChars' => TransposedCharTyposGenerator::class,
        'doubleChars' => DoubleCharTyposGenerator::class,
        'flipBits' => BitFlippingTyposGenerator::class,
        'generateHomophones' => HomophoneTyposGenerator::class,
    ];


    /**
     * The array of generated typos.
     *
     * @var array
     */
    private $typos = [];

    /**
     * The original word.
     *
     * @var string
     */
    private $word;


    /**
     * Constructor.
     *
     * Options example (options set to false can be ommited):<br/>
     * <code>
     * $options = [<br/>
     *     'wrongKeys' => true,<br/>
     *     'missedChars' => true,<br/>
     *     'transposedChars' => false,<br/>
     *     'doubleChars' => false,<br/>
     *     'flipBits' => true,<br/>
     *     'generateHomophones' => false,<br/>
     * ];
     *
     * @param array $options the options to use for the typo generation
     */
    public function __construct(array $options = [])
    {
        $resolver = new OptionsResolver();
        $this->configureOptions($resolver);
        $this->options = $resolver->resolve($options);
    }

    private function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults($this->options);
    }

    /**
     * Merge all the typos depending on configuration.
     *
     * @param string $word the original word from which the typos will be generated
     *
     * @return TypoGenerator the generator itself
     */
    private function generate(): Traversable
    {
        $this->word = strtolower($this->word);

        foreach ($this->options as $key => $active) {
            if (!$active) {
                continue;
            }
            $generator = $this->generatorMap[$key];
            yield from $generator::getTypos($this->word);
        }
    }

    /**
     * Get the generated typos.
     */
    private function makeTyposAsArray(): void
    {
        $this->typos = iterator_to_array($this->generate(), false);
    }

    public function getTypos($word)
    {
        $this->word = $word;
        $this->makeTyposAsArray();
        $this->typos = array_unique($this->typos);
        if (($key = array_search($this->word, $this->typos)) !== false) {
            unset($this->typos[$key]);
        }

        return $this->typos;
    }
}
